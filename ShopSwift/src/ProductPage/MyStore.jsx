import React, { useEffect, useState } from 'react';
import { collection, getDocs, getFirestore, query, where } from 'firebase/firestore';
import { Link,useParams } from 'react-router-dom';
import ClothingCard from "../components/ClothingCard.jsx";
import SearchInput from "../components/SearchInput.jsx";
import MaxPriceInput from "../components/MaxPriceInput.jsx";
import ClothingTypeSelect from "../components/ClothingTypeSelect.jsx";
import SizeSelect from "../components/SizeSelect.jsx";
import 'bulma/css/bulma.min.css';
import {getAuth} from "firebase/auth";
import GenderSelect from "../components/Gender.jsx";

const Home = () => {
    const { id } = useParams();
    const [clothes, setClothes] = useState([]);
    const [searchTerm, setSearchTerm] = useState('');
    const [maxPrice, setMaxPrice] = useState('');
    const [clothingType, setClothingType] = useState('');
    const [size, setSize] = useState('');
    const [isShoe, setIsShoe] = useState(false);
    const [Gender, setGender] = useState('');
    // Handler function for clothing type change
    const handleClothingTypeChange = (e) => {
        const selectedClothingType = e.target.value;
        setClothingType(selectedClothingType);
        // Check if shoes are selected
        setIsShoe(selectedClothingType === 'Chaussures');
    };

    const handleGenderChange = (e) => {
        const selectedGender = e.target.value;
        setGender(selectedGender);
    
      };
console.log(id)
    useEffect(() => {
        // Fetch clothes data from Firestore based on filters
        const fetchClothes = async () => {
            try {
                const auth = getAuth(); // Get the auth instance
                const user = auth.currentUser; // Get the current user

                if (user) {
                    const db = getFirestore();
                    const clothesCollection = collection(db, 'vetements');

                    let clothesQuery = query(
                        clothesCollection,
                        where('userId', '==', user.uid)
                    );

                    if (searchTerm) {
                        const searchTermLower = searchTerm.toLowerCase();
                        clothesQuery = query(clothesQuery, where('productName', '==', searchTermLower));
                    }

                    if (maxPrice) {
                        const maxPriceNumber = parseFloat(maxPrice);
                        clothesQuery = query(clothesQuery, where('price', '<=', maxPriceNumber));
                    }

                    if (clothingType) {
                        clothesQuery = query(clothesQuery, where('clothingType', '==', clothingType));
                    }

                    if (size) {
                        clothesQuery = query(clothesQuery, where('size', '==', size));
                    }

                    if (Gender) {
                        clothesQuery = query(clothesQuery, where('gender', '==', Gender));
                      }
              
                      clothesQuery = query(clothesQuery, where('vendu', '==', false));

                    const clothesSnapshot = await getDocs(clothesQuery);
                    const clothesData = clothesSnapshot.docs.map((doc) => ({
                        id: doc.id,
                        ...doc.data(),
                    }));

                    setClothes(clothesData);

                }
            } catch (error) {
                console.error('Error fetching clothes:', error.message);
            }
        };

        fetchClothes();
    }, [searchTerm, maxPrice, clothingType, size,Gender]); // Update data when filters change


    return (
        <div className="container">
            {/* Search filters */}
            <div className="columns">
                <SearchInput value={searchTerm} onChange={(e) => setSearchTerm(e.target.value)} />
                <MaxPriceInput value={maxPrice} onChange={(e) => setMaxPrice(e.target.value)} />
                <ClothingTypeSelect value={clothingType} onChange={handleClothingTypeChange} />
                <SizeSelect value={size} onChange={(e) => setSize(e.target.value)} isShoe={isShoe} />
                <GenderSelect value={Gender} onChange={handleGenderChange} />
            </div>

            {/* Clothes display */}
            <div className="columns is-multiline">
                {clothes.map((clothing) => (
                    <div key={clothing.id} className="column is-one-third">
                        <Link to={`/product/${clothing.id}`}>
                            <ClothingCard clothing={clothing} />
                        </Link>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default Home;
