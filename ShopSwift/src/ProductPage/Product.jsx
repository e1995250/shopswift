import React, { useEffect, useState } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { arrayUnion, arrayRemove, doc, getDoc, getFirestore, updateDoc } from 'firebase/firestore';
import { getAuth, onAuthStateChanged } from 'firebase/auth';
import 'bulma/css/bulma.min.css';

const ProductPage = () => {
  const { id } = useParams();
  const [product, setProduct] = useState(null);
  const [currentUser, setCurrentUser] = useState(null);
  const [notification, setNotification] = useState(null);
  const [isInCart, setIsInCart] = useState(false);
  const navigation = useNavigate();
  const [currentImageIndex, setCurrentImageIndex] = useState(0);

  useEffect(() => {
    const fetchProduct = async () => {
      try {
        const auth = getAuth();
        onAuthStateChanged(auth, (user) => {
          setCurrentUser(user);
        });

        const db = getFirestore();
        const productRef = doc(db, 'vetements', id);
        const productDoc = await getDoc(productRef);

        if (productDoc.exists()) {
          setProduct({ id: productDoc.id, ...productDoc.data() });
        } else {
          console.log('Aucun tel document !');
        }
      } catch (error) {
        console.error('Erreur lors de la récupération des détails du produit :', error.message);
      }
    };

    fetchProduct();
  }, [id]);

  useEffect(() => {
    const checkCart = async () => {
      try {
        if (!currentUser) return;

        const db = getFirestore();
        const cartRef = doc(db, 'Panier', currentUser.uid);
        const cartDoc = await getDoc(cartRef);

        if (cartDoc.exists()) {
          const cartProducts = cartDoc.data().products || [];
          setIsInCart(cartProducts.includes(id));
        }
      } catch (error) {
        console.error('Erreur lors de la vérification du panier :', error.message);
      }
    };

    checkCart();
  }, [currentUser, id]);

  const addToCart = async () => {
    try {
      const db = getFirestore();
      const cartRef = doc(db, 'Panier', currentUser.uid);

      await updateDoc(cartRef, {
        products: arrayUnion(id),
      });

      setNotification('Produit ajouté au panier !');

      setTimeout(() => {
        setNotification(null);
      }, 3000);

      console.log('Produit ajouté au panier !');
      setIsInCart(true);
    } catch (error) {
      console.error('Erreur lors de l\'ajout du produit au panier :', error.message);
    }
  };

  const removeFromCart = async () => {
    try {
      const db = getFirestore();
      const cartRef = doc(db, 'Panier', currentUser.uid);

      await updateDoc(cartRef, {
        products: arrayRemove(id),
      });

      console.log('Produit retiré du panier !');
      setIsInCart(false);
    } catch (error) {
      console.error('Erreur lors de la suppression du produit du panier :', error.message);
    }
  };

  const currentUserId = currentUser ? currentUser.uid : null;
  const isCurrentUserProduct = product && product.userId === currentUserId;

  const nextImage = () => {
    setCurrentImageIndex((prevIndex) => (prevIndex + 1) % product.imagesUrls.length);
  };

  const prevImage = () => {
    setCurrentImageIndex((prevIndex) =>
      prevIndex === 0 ? product.imagesUrls.length - 1 : prevIndex - 1
    );
  };

  return (
    <div className="container">
      <h1 className="title is-2 has-text-centered">Page du produit</h1>

      {notification && (
        <div className="notification is-success">
          <button className="delete" onClick={() => setNotification(null)}></button>
          {notification}
        </div>
      )}

      {product ? (
        <div className="columns">
          <div className="column is-half">
            <div className="product-images">
              <div className="product-image">
                <img
                  src={product.imagesUrls[currentImageIndex]}
                  alt={`Produit ${currentImageIndex + 1}`}
                  style={{ width: '700px', height: '600px' }}
                />
              </div>
              {product.imagesUrls.length > 1 && (
                <div className="image-arrows">
                  <button className="arrow-button" onClick={prevImage}>
                    &lt; Précédent
                  </button>
                  <button className="arrow-button" onClick={nextImage}>
                    Suivant &gt;
                  </button>
                </div>
              )}
            </div>
          </div>
          <div className="column is-half">
            <div className="card">
              <div className="card-content">
                <h2 className="title is-1 has-text-centered">{product.productName}</h2>
                {product.userId && (
                  <div className="has-text-centered mt-2">
                    <Link to={`/store/${product.userId}`}>Voir les produits du vendeur</Link>
                  </div>
                )}
                <p className="subtitle is-6">{product.description}</p>
                <div className="columns">
                  <div className="column is-one-third">
                    <p className="is-size-4">Type : {product.clothingType}</p>
                  </div>
                  <div className="column is-one-third">
                    <p className="is-size-4">Genre : {product.gender}</p>
                  </div>
                  <div className="column is-one-third">
                    <p className="is-size-4">Taille : {product.size}</p>
                  </div>
                </div>
                <p className="is-size-2">{product.price}$</p>
                <div className="buttons mt-4">
                  {isCurrentUserProduct ? (
                    <>
                      <Link to={`/modify-product/${id}`} className="button is-primary mr-2">
                        Modifier
                      </Link>
                      <button className="button is-danger" onClick={removeFromCart}>
                        Supprimer
                      </button>
                    </>
                  ) : (
                    <button
                      className={`button ${isInCart ? 'is-danger' : 'is-success'}`}
                      onClick={isInCart ? removeFromCart : addToCart}
                    >
                      {isInCart ? 'Retirer du panier' : 'Ajouter au panier'}
                    </button>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <p>Chargement...</p>
      )}
    </div>
  );
};

export default ProductPage;
