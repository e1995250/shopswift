import React, { useState } from 'react';
import { getAuth } from 'firebase/auth';
import {
  getFirestore,
  collection,
  addDoc,
  serverTimestamp,
} from 'firebase/firestore';
import { getStorage, ref, uploadBytes, getDownloadURL } from 'firebase/storage';
import 'bulma/css/bulma.min.css';
import { useNavigate } from 'react-router-dom';

const Vendre = () => {
  // State to manage form inputs
  const [productName, setProductName] = useState('');
  const [clothingType, setClothingType] = useState('');
  const [size, setSize] = useState('');
  const [price, setPrice] = useState('');
  const [images, setImages] = useState([]);
  const [description, setDescription] = useState('');
  const [gender, setGender] = useState(''); // New state for gender
  const navigation = useNavigate();

  // Function to handle image file change
  const handleImageChange = (e) => {
    const selectedImages = Array.from(e.target.files);
    setImages([...images, ...selectedImages]); // Append new images to existing array
  };

  // Function to remove an image from the array
  const removeImage = (index) => {
    const updatedImages = [...images];
    updatedImages.splice(index, 1); // Remove the image at the specified index
    setImages(updatedImages);
  };

  // Function to handle form submission
  const handleSell = async () => {
    try {
      const auth = getAuth();
      const user = auth.currentUser;

      if (!user) {
        console.error('User not authenticated.');
        return;
      }

      const db = getFirestore();
      const storage = getStorage();

      // Upload images to Firebase Storage
      const imagesUrls = await Promise.all(
          images.map(async (image) => {
            const storageRef = ref(storage, `images/${user.uid}/${image.name}`);
            await uploadBytes(storageRef, image);
            return getDownloadURL(storageRef);
          })
      );

      const priceNumber = parseInt(price);
      // Create a new document in "vetements" collection
      const newVetementRef = await addDoc(collection(db, 'vetements'), {
        productName,
        clothingType,
        size,
        price: priceNumber,
        description,
        imagesUrls, // Store the image URLs
        createdAt: serverTimestamp(),
        userId: user.uid,
        gender,
        vendu: false // Include gender in the document
      });

      console.log('Vetement added with ID:', newVetementRef.id);
      navigation('/home');
    } catch (error) {
      console.error('Error adding vetement:', error.message);
    }
  };

  // Function to handle form cancelation
  const handleCancel = () => {
    // Implement logic to handle cancel action
    console.log('Sell canceled');
    navigation('/home');
  };

  // Render US sizes if "Chaussures" is selected
  const renderSizeOptions = () => {
    if (clothingType === 'Chaussures') {
      return (
          <>
            <option value="7">7 (US)</option>
            <option value="7.5">7.5 (US)</option>
            <option value="8">8 (US)</option>
            <option value="8.5">8.5 (US)</option>
            <option value="9">9 (US)</option>
            <option value="9.5">9.5 (US)</option>
            <option value="10">10 (US)</option>
            <option value="10.5">10.5 (US)</option>
            <option value="11">11 (US)</option>
            <option value="11.5">11.5 (US)</option>
            <option value="12">12 (US)</option>
            <option value="12.5">12.5 (US)</option>
          </>
      );
    } else {
      return (
          <>
            <option value="XS">XS</option>
            <option value="S">S</option>
            <option value="M">M</option>
            <option value="L">L</option>
            <option value="XL">XL</option>
          </>
      );
    }
  };

  return (
      <div className="container mt-5">
        <h1 className="title is-3">Vendre</h1>
        <form>
          <div className="field">
            <label className="label">Nom du produit</label>
            <div className="control">
              <input
                  className="input"
                  type="text"
                  value={productName}
                  onChange={(e) => setProductName(e.target.value)}
              />
            </div>
          </div>
          <div className="field">
            <label className="label">Type de vêtement</label>
            <div className="control">
              <div className="select">
                <select
                    value={clothingType}
                    onChange={(e) => setClothingType(e.target.value)}
                >
                  <option value="">Sélectionner le type</option>
                  <option value="Pantalon">Pantalon</option>
                  <option value="Chemise">Chemise</option>
                  <option value="Robe">Robe</option>
                  <option value="Veste">Veste</option>
                  <option value="Pull">Pull</option>
                  <option value="Jupe">Jupe</option>
                  <option value="Shorts">Shorts</option>
                  <option value="Blouse">Blouse</option>
                  <option value="Manteau">Manteau</option>
                  <option value="Écharpe">Écharpe</option>
                  <option value="Gants">Gants</option>
                  <option value="Chapeau">Chapeau</option>
                  <option value="Chaussettes">Chaussettes</option>
                  <option value="Chaussures">Chaussures</option>
                </select>
              </div>
            </div>
          </div>

          {/* New area to choose gender */}
          <div className="field">
            <label className="label">Genre</label>
            <div className="control">
              <div className="select">
                <select
                    value={gender}
                    onChange={(e) => setGender(e.target.value)}
                >
                  <option value="">Sélectionner le genre</option>
                  <option value="Homme">Homme</option>
                  <option value="Femme">Femme</option>
                  <option value="Unisexe">Unisexe</option>
                </select>
              </div>
            </div>
          </div>

          <div className="field">
            <label className="label">Taille</label>
            <div className="control">
              <div className="select">
                <select
                    value={size}
                    onChange={(e) => setSize(e.target.value)}
                >
                  <option value="">Sélectionner la taille</option>
                  {renderSizeOptions()}
                </select>
              </div>
            </div>
          </div>

          <div className="field">
            <label className="label">Prix</label>
            <div className="control">
              <input
                  className="input"
                  type="number"
                  value={price}
                  onChange={(e) => setPrice(e.target.value)}
              />
            </div>
          </div>
          <div className="field">
            <label className="label">Description</label>
            <div className="control">
            <textarea
                className="textarea"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
            ></textarea>
            </div>
          </div>

          {/* New input field for multiple image selection */}
          <div className="field">
            <label className="label">Images</label>
            <div className="control">
              <input
                  className="input"
                  type="file"
                  accept="image/*"
                  onChange={handleImageChange}
                  multiple // Allow multiple file selection
              />
            </div>
          </div>

          {/* Display selected files */}
          <div className="field">
            <label className="label">Selected Files:</label>
            <ul>
              {images.map((file, index) => (
                  <li key={index}>
                    {file.name}
                    <button
                        className="delete is-small ml-2"
                        onClick={() => removeImage(index)} // Remove the image at the corresponding index
                    ></button>
                  </li>
              ))}
            </ul>
          </div>

          <div className="field is-grouped">
            <div className="control">
              <button
                  className="button is-success"
                  type="button"
                  onClick={handleSell}
              >
                Vendre
              </button>
            </div>
            <div className="control">
              <button
                  className="button is-danger"
                  type="button"
                  onClick={handleCancel}
              >
                Annuler
              </button>
            </div>
          </div>
        </form>
      </div>
  );
};

export default Vendre;
