import React, { useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { getFirestore, doc, getDoc, updateDoc } from 'firebase/firestore';
import { getAuth } from 'firebase/auth';
import { getStorage, ref, uploadBytes, deleteObject, getDownloadURL } from 'firebase/storage';

import 'bulma/css/bulma.min.css';

const ModifyProduct = () => {
    const { id } = useParams();
    const [productName, setProductName] = useState('');
    const [clothingType, setClothingType] = useState('');
    const [size, setSize] = useState('');
    const [price, setPrice] = useState('');
    const [description, setDescription] = useState('');
    const [images, setImages] = useState([]);
    const [gender, setGender] = useState('');
    const [newImages, setNewImages] = useState([]);
    const [fileNames, setFileNames] = useState([]); // State to store file names
    const navigation = useNavigate();

    useEffect(() => {
        const fetchProduct = async () => {
            try {
                const db = getFirestore();
                const productRef = doc(db, 'vetements', id);
                const productDoc = await getDoc(productRef);

                if (productDoc.exists()) {
                    const data = productDoc.data();
                    setProductName(data.productName);
                    setClothingType(data.clothingType);
                    setSize(data.size);
                    setPrice(data.price.toString());
                    setDescription(data.description);
                    setGender(data.gender);
                    setImages(data.imagesUrls); // Set existing images
                } else {
                    console.log('No such document!');
                }
            } catch (error) {
                console.error('Error fetching product details:', error.message);
            }
        };

        fetchProduct();
    }, [id]);

    const handleImageChange = (e) => {
        const selectedImages = Array.from(e.target.files);
        setNewImages([...newImages, ...selectedImages]);

        // Extract file names and update fileNames state
        const names = selectedImages.map(image => image.name);
        setFileNames([...fileNames, ...names]);
    };

    const removeImage = async (index) => {
        try {
            // Remove image from storage if it's a new image
            if (index >= images.length) {
                const updatedImages = [...newImages];
                updatedImages.splice(index - images.length, 1);
                setNewImages(updatedImages);
            } else {
                // Remove image from storage
                const storage = getStorage();
                const imageRef = ref(storage, images[index]);
                await deleteObject(imageRef);

                // Remove image URL from imagesUrls array in Firestore
                const db = getFirestore();
                const productRef = doc(db, 'vetements', id);
                const productDoc = await getDoc(productRef);

                if (productDoc.exists()) {
                    const { imagesUrls } = productDoc.data();
                    const updatedImagesUrls = imagesUrls.filter((url, i) => i !== index);
                    await updateDoc(productRef, { imagesUrls: updatedImagesUrls });
                }

                // Remove image from state
                const updatedImages = [...images];
                updatedImages.splice(index, 1);
                setImages(updatedImages);
            }
        } catch (error) {
            console.error('Error deleting image:', error.message);
        }
    };

    const removeSelectedFile = (index) => {
        const updatedFileNames = [...fileNames];
        updatedFileNames.splice(index, 1);
        setFileNames(updatedFileNames);

        const updatedNewImages = [...newImages];
        updatedNewImages.splice(index, 1);
        setNewImages(updatedNewImages);
    };


    const handleModify = async () => {
        try {
            const auth = getAuth();
            const user = auth.currentUser;

            if (!user) {
                console.error('User not authenticated.');
                return;
            }

            const db = getFirestore();
            const storage = getStorage();

            // Upload new images
            const newImagesUrls = await Promise.all(
                newImages.map(async (image) => {
                    const storageRef = ref(storage, `images/${user.uid}/${image.name}`);
                    await uploadBytes(storageRef, image);
                    return getDownloadURL(storageRef);
                })
            );

            // Concatenate existing and new images
            const allImages = [...images, ...newImagesUrls];

            const priceNumber = parseInt(price);
            const productRef = doc(db, 'vetements', id);
            await updateDoc(productRef, {
                productName,
                clothingType,
                size,
                price: priceNumber,
                description,
                imagesUrls: allImages,
                gender,
            });

            console.log('Product modified successfully!');
            navigation(`/product/${id}`);
        } catch (error) {
            console.error('Error modifying product:', error.message);
        }
    };

    const handleCancel = () => {
        navigation(`/product/${id}`);
    };

    // Render size options based on clothing type
    const renderSizeOptions = () => {
        if (clothingType === 'Chaussures') {
            return (
                <>
                    <option value="7">7 (US)</option>
                    <option value="7.5">7.5 (US)</option>
                    <option value="8">8 (US)</option>
                    <option value="8.5">8.5 (US)</option>
                    <option value="9">9 (US)</option>
                    <option value="9.5">9.5 (US)</option>
                    <option value="10">10 (US)</option>
                    <option value="10.5">10.5 (US)</option>
                    <option value="11">11 (US)</option>
                    <option value="11.5">11.5 (US)</option>
                    <option value="12">12 (US)</option>
                    <option value="12.5">12.5 (US)</option>
                </>
            );
        } else {
            return (
                <>
                    <option value="XS">XS</option>
                    <option value="S">S</option>
                    <option value="M">M</option>
                    <option value="L">L</option>
                    <option value="XL">XL</option>
                </>
            );
        }
    };

    return (
        <div className="container mt-5">
            <h1 className="title is-3">Modifier le produit</h1>
            <form>
                <div className="field">
                    <label className="label">Nom du produit</label>
                    <input
                        className="input"
                        type="text"
                        value={productName}
                        onChange={(e) => setProductName(e.target.value)}
                    />
                </div>
                <div className="field">
                    <label className="label">Type de vêtement</label>
                    <div className="control">
                        <div className="select">
                            <select
                                value={clothingType}
                                onChange={(e) => setClothingType(e.target.value)}
                            >
                                <option value="">Sélectionner le type</option>
                                <option value="Pantalon">Pantalon</option>
                                <option value="Chemise">Chemise</option>
                                <option value="Robe">Robe</option>
                                <option value="Veste">Veste</option>
                                <option value="Pull">Pull</option>
                                <option value="Jupe">Jupe</option>
                                <option value="Shorts">Shorts</option>
                                <option value="Blouse">Blouse</option>
                                <option value="Manteau">Manteau</option>
                                <option value="Écharpe">Écharpe</option>
                                <option value="Gants">Gants</option>
                                <option value="Chapeau">Chapeau</option>
                                <option value="Chaussettes">Chaussettes</option>
                                <option value="Chaussures">Chaussures</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div className="field">
                    <label className="label">Genre</label>
                    <div className="control">
                        <div className="select">
                            <select
                                value={gender}
                                onChange={(e) => setGender(e.target.value)}
                            >
                                <option value="">Sélectionner le genre</option>
                                <option value="Homme">Homme</option>
                                <option value="Femme">Femme</option>
                                <option value="Unisexe">Unisexe</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div className="field">
                    <label className="label">Taille</label>
                    <div className="control">
                        <div className="select">
                            <select
                                value={size}
                                onChange={(e) => setSize(e.target.value)}
                            >
                                <option value="">Sélectionner la taille</option>
                                {renderSizeOptions()}
                                {/* Add more options as needed */}
                            </select>
                        </div>
                    </div>
                </div>
                <div className="field">
                    <label className="label">Prix</label>
                    <input
                        className="input"
                        type="text"
                        value={price}
                        onChange={(e) => setPrice(e.target.value)}
                    />
                </div>
                <div className="field">
                    <label className="label">Description</label>
                    <input
                        className="input"
                        type="text"
                        value={description}
                        onChange={(e) => setDescription(e.target.value)}
                    />
                </div>
                {/* Existing images */}
                {images.map((imageUrl, index) => (
                    <div key={index} className="mb-3">
                        <img src={imageUrl} alt={`Product ${index + 1}`} className="mb-2" style={{ maxWidth: '200px' }} />
                        <button type="button" className="button is-danger" onClick={() => removeImage(index)}>Delete</button>
                    </div>
                ))}

                {/* New images */}
                <div className="field">
                    <label className="label">Rajouter des nouvelles images</label>
                    <input type="file" multiple onChange={handleImageChange} />
                </div>

                {fileNames.length > 0 && (
                    <div className="field">
                        <label className="label">Fichier sélectionné:</label>
                        <ul>
                            {fileNames.map((fileName, index) => (
                                <li key={index}>
                                    {fileName}
                                    <button className="delete is-small ml-2" onClick={() => removeSelectedFile(index)}></button>
                                </li>
                            ))}
                        </ul>
                    </div>
                )}


                <div className="field is-grouped">
                    <div className="control">
                        <button className="button is-success" type="button" onClick={handleModify}>
                            Modifier
                        </button>
                    </div>
                    <div className="control">
                        <button className="button is-danger" type="button" onClick={handleCancel}>
                            Annuler
                        </button>
                    </div>
                </div>
            </form>
        </div>
    );
};

export default ModifyProduct;
