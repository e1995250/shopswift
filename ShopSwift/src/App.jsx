import React, { useEffect } from 'react';
import { BrowserRouter as Router, Route, Routes, Navigate } from 'react-router-dom';
import Login from './ConexionPages/Login';
import Signup from './ConexionPages/Signup';
import Home from './Home/home';
import Navbar from './Home/Navbar';
import Vendre from './CRUD products/Vendre.jsx';
import Settings from './Settings/Settings';
import { onAuthStateChanged } from 'firebase/auth';
import { auth } from './index.jsx';
import Product from './ProductPage/Product';
import CartPage from './Panier/panier';
import ModifyProduct from './CRUD products/ModifyProduct';
import MyStore from './ProductPage/MyStore';
import Store from './ProductPage/Store';
import Achats from './Transactions/achats';
import Ventes from './Transactions/ventes';
import Checkout from './Panier/checkout.jsx';
import Paypal from './components/paypal.jsx'; // Import the Paypal component

const App = () => {
  useEffect(() => {
    const unlisten = onAuthStateChanged(auth, (user) => {
      const isUserAuthenticated = !!user;

      if (!isUserAuthenticated && window.location.pathname !== '/login' && window.location.pathname !== '/signup') {
        // If user is not authenticated and trying to access a protected route,
        // Redirect to login page
        window.location.href = '/login';
      }
    });

    return () => {
      unlisten(); // Clean up the listener when component unmounts
    };
  }, []);

  return (
      <Router>
        <div>
          <Navbar />
          <Routes>
            <Route path="/login" element={<Login />} />
            <Route path="/signup" element={<Signup />} />
            <Route path="/home" element={<Home />} />
            <Route path="/vendre" element={<Vendre />} />
            <Route path="/settings" element={<Settings />} />
            <Route path="/product/:id" element={<Product />} />
            <Route path="/cart" element={<CartPage />} />
            <Route path={'/modify-product/:id'} element={<ModifyProduct/>} />
            <Route path={'/mystore'} element={<MyStore/>} />
            <Route path={'/store/:id'} element={<Store/>} />
            <Route path={'/checkout'} element={<Checkout/>} />
            {/* Include Paypal component */}
            <Route path="/" element={<Paypal />} />
            <Route path={'/mes_achats'} element={<Achats/>} />
            <Route path={'/mes_ventes'} element={<Ventes/>} />
            {/* Redirect from / to /login */}
            <Route path="/" element={<Navigate to="/login" />} />
          </Routes>
        </div>
      </Router>
  );
};

export default App;
