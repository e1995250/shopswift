import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { getFirestore, collection, query, where, getDocs, doc, updateDoc,getDoc  } from 'firebase/firestore';
import { getAuth, onAuthStateChanged } from 'firebase/auth';
import 'bulma/css/bulma.min.css';

const Ventes = () => {
  const [transactions, setTransactions] = useState([]);
  const [currentUser, setCurrentUser] = useState(null);
  const [productDetails, setProductDetails] = useState({});
  const [modifiedTrackingNumber, setModifiedTrackingNumber] = useState('');
  const [successMessage, setSuccessMessage] = useState('');

  useEffect(() => {
    const fetchTransactions = async () => {
      try {
        const auth = getAuth();
        const user = await new Promise((resolve) => {
          const unsubscribe = onAuthStateChanged(auth, (user) => {
            resolve(user);
            unsubscribe();
          });
        });

        setCurrentUser(user);

        if (user) {
          const db = getFirestore();
          const transactionsCollection = collection(db, 'Transactions');

          const transactionsQuery = query(
            transactionsCollection,
            where('sellerId', '==', user.uid)
          );

          const transactionsSnapshot = await getDocs(transactionsQuery);
          const transactionsData = transactionsSnapshot.docs.map((doc) => ({
            id: doc.id,
            ...doc.data(),
          }));

          const sortedTransactions = transactionsData.sort(
            (a, b) => (a.done ? 1 : 0) - (b.done ? 1 : 0)
          );
          setTransactions(sortedTransactions);
        }
      } catch (error) {
        console.error('Error fetching transactions:', error.message);
      }
    };

    fetchTransactions();
  }, []);

  useEffect(() => {
    const fetchProductDetails = async () => {
      try {
        if (transactions.length > 0) {
          const db = getFirestore();

          const detailsPromises = transactions.map(async (transaction) => {
            const productRef = doc(db, 'vetements', transaction.productId);
            const productDoc = await getDoc(productRef);

            return {
              transactionId: transaction.id,
              shiping: transaction.shippingDetails,
              productDetails: productDoc.exists() ? productDoc.data() : null,
            };
          });

          const productDetailsData = await Promise.all(detailsPromises);
          setProductDetails(productDetailsData);
        }
      } catch (error) {
        console.error('Error fetching product details:', error.message);
      }
    };

    fetchProductDetails();
  }, [transactions]);

  const handleTrackingNumberChange = async (transactionId) => {
    try {
      const db = getFirestore();
      const transactionRef = doc(db, 'Transactions', transactionId);

      // Update the tracking number in the database
      await updateDoc(transactionRef, {
        tracking_number: modifiedTrackingNumber,
      });

      // Display success message
      setSuccessMessage('Numéro de suivi mis à jour avec succès');

      // Clear the modified tracking number state after 3 seconds
      setTimeout(() => {
        setSuccessMessage('');
      }, 3000);
    } catch (error) {
      console.error('Error updating tracking number:', error.message);
    }
  };

  return (
    <div className="container">
      <h1 className="title is-2">Page de transaction</h1>
      {currentUser ? (
        <div>
          {productDetails.length > 0 ? (
            <div>
              <h2 className="subtitle is-4">Vos ventes:</h2>
              {productDetails.map(({ transactionId, productDetails,shiping }) => (
                <div key={transactionId} className="card">
                  <div className="card-content">
                    {productDetails ? (
                      <>
                        <p>Nom de produit: {productDetails.productName}</p>
                        <p>Prix: ${productDetails.price}</p>
                        <p>Transaction ID: {transactionId}</p>
                        <p>adresse d'envoie: {shiping}</p>
                        <p>Numéro de suivi:</p>
                        <div className="field is-grouped">
                          <p className="control is-expanded">
                            <input
                              className="input"
                              type="text"
                              value={modifiedTrackingNumber || transactions.find(t => t.id === transactionId)?.tracking_number}
                              onChange={(e) => setModifiedTrackingNumber(e.target.value)}
                              placeholder={transactions.find(t => t.id === transactionId)?.tracking_number}
                            />
                          </p>
                          <p className="control">
                            <button
                              className="button is-primary"
                              onClick={() => handleTrackingNumberChange(transactionId)}
                            >
                              mettre à jour
                            </button>
                          </p>
                        </div>
                        {successMessage && <span>{successMessage}</span>}
                      </>
                    ) : (
                      <p>Détails du produit non disponibles.</p>
                    )}
                  </div>
                </div>
              ))}
            </div>
          ) : (
            <p>Vous n'avez encore effectué aucune transaction.</p>
          )}
        </div>
      ) : (
        <p>Veuillez vous connecter pour consulter vos transactions.</p>
      )}
    </div>
  );
};

export default Ventes;
