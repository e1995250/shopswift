import React, { useState, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { getFirestore, collection, query, where, getDocs, doc, getDoc, updateDoc } from 'firebase/firestore';
import { getAuth, onAuthStateChanged } from 'firebase/auth';
import 'bulma/css/bulma.min.css';

const Achats = () => {
  const [transactions, setTransactions] = useState([]);
  const [currentUser, setCurrentUser] = useState(null);
  const [productDetails, setProductDetails] = useState({});
  const [confirmedTransactionId, setConfirmedTransactionId] = useState(null);

  useEffect(() => {
    const fetchTransactions = async () => {
      try {
        const auth = getAuth();
        const user = await new Promise((resolve) => {
          const unsubscribe = onAuthStateChanged(auth, (user) => {
            resolve(user);
            unsubscribe();
          });
        });

        setCurrentUser(user);

        if (user) {
          const db = getFirestore();
          const transactionsCollection = collection(db, 'Transactions');

          const transactionsQuery = query(
            transactionsCollection,
            where('buyerId', '==', user.uid)
          );

          const transactionsSnapshot = await getDocs(transactionsQuery);
          const transactionsData = transactionsSnapshot.docs.map((doc) => ({
            id: doc.id,
            ...doc.data(),
          }));

          const sortedTransactions = transactionsData.sort(
            (a, b) => (a.done ? 1 : 0) - (b.done ? 1 : 0)
          );
          setTransactions(sortedTransactions);
        }
      } catch (error) {
        console.error('Error fetching transactions:', error.message);
      }
    };

    fetchTransactions();
  }, []);

  useEffect(() => {
    const fetchProductDetails = async () => {
      try {
        if (transactions.length > 0) {
          const db = getFirestore();

          const detailsPromises = transactions.map(async (transaction) => {
            const productRef = doc(db, 'vetements', transaction.productId);
            const productDoc = await getDoc(productRef);

            return {
              transactionId: transaction.id,
              productDetails: productDoc.exists() ? productDoc.data() : null,
            };
          });

          const productDetailsData = await Promise.all(detailsPromises);
          setProductDetails(productDetailsData);
        }
      } catch (error) {
        console.error('Error fetching product details:', error.message);
      }
    };

    fetchProductDetails();
  }, [transactions]);

  useEffect(() => {
    if (confirmedTransactionId) {
      setTransactions((prevTransactions) => {
        const updatedTransactions = [...prevTransactions];
        const index = updatedTransactions.findIndex((t) => t.id === confirmedTransactionId);

        if (index !== -1) {
          const movedTransaction = updatedTransactions.splice(index, 1)[0];
          updatedTransactions.push(movedTransaction);
        }

        return updatedTransactions;
      });

      setConfirmedTransactionId(null);
    }
  }, [confirmedTransactionId]);

  const handleConfirmation = async (transactionId) => {
    try {
      const db = getFirestore();
      const transactionRef = doc(db, 'Transactions', transactionId);

      // Set the "done" field to true
      await updateDoc(transactionRef, {
        done: true,
      });

      // Update the state locally
      setTransactions((prevTransactions) => {
        const updatedTransactions = [...prevTransactions];
        const index = updatedTransactions.findIndex((t) => t.id === transactionId);

        if (index !== -1) {
          const movedTransaction = updatedTransactions.splice(index, 1)[0];
          movedTransaction.done = true; // Update the done field locally
          updatedTransactions.push(movedTransaction);
        }

        return updatedTransactions;
      });

      // Set the confirmed transactionId after a short delay
      setTimeout(() => {
        setConfirmedTransactionId(transactionId);
      }, 1000);

      console.log(`Transaction with ID ${transactionId} confirmed.`);
    } catch (error) {
      console.error('Error confirming transaction:', error.message);
    }
  };


  return (
    <div className="container">
  <h1 className="title is-2">Page de transaction</h1>
  {currentUser ? (
    <div>
      {productDetails.length > 0 ? (
        <div>
          <h2 className="subtitle is-4">Vos achats:</h2>
          {productDetails.map(({ transactionId, productDetails }) => (
            <div key={transactionId} className="card">
              <div className="card-content">
                {productDetails ? (
                  <>
                    <p>Nom de produit: {productDetails.productName}</p>
                    <p>Prix: ${productDetails.price}</p>
                    {productDetails.userId && (
                      <div className="text mt-2">
                        <Link to={`/store/${productDetails.userId}`}>Tout les produits du vendeur</Link>
                      </div>
                    )}
                    <p>Transaction ID: {transactionId}</p>
                    <p>Numéro de suivi: {transactions.find(t => t.id === transactionId)?.tracking_number}</p> {/* Access tracking number from transaction */}
                    {!transactions.find(t => t.id === transactionId)?.done && (
                      <button
                        className="button is-primary"
                        onClick={() => handleConfirmation(transactionId)}
                      >
                        Confirmer la réception
                      </button>
                    )}
                  </>
                ) : (
                  <p>Détails du produit non disponibles.</p>
                )}
              </div>
            </div>
          ))}
        </div>
      ) : (
        <p>Vous n'avez encore effectué aucune transaction.</p>
      )}
    </div>
  ) : (
    <p>Veuillez vous connecter pour consulter vos transactions.</p>
  )}
</div>

  );
};

export default Achats;
