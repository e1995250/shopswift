import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { getAuth, EmailAuthProvider, updateEmail, updatePassword, updateProfile, reauthenticateWithCredential } from 'firebase/auth';

const ModifyProfileSettings = () => {
    const auth = getAuth();
    const [email, setEmail] = useState(auth.currentUser.email);
    const [displayName, setDisplayName] = useState(auth.currentUser.displayName);
    const [currentPassword, setCurrentPassword] = useState('');
    const [newPassword, setNewPassword] = useState('');
    const [profilePicUrl, setProfilePicUrl] = useState(auth.currentUser.photoURL);
    const navigate = useNavigate();

    const reauthenticate = async () => {
        const user = auth.currentUser;
        const credential = EmailAuthProvider.credential(user.email, currentPassword);

        try {
            await reauthenticateWithCredential(user, credential);
        } catch (error) {
            console.error(error);
            // Handle reauthentication error
            throw new Error('Reauthentication failed');
        }
    };

    const modifyProfile = async (e) => {
        e.preventDefault();
        try {
            await reauthenticate();

            // Update email
            await updateEmail(auth.currentUser, email);

            // Update display name
            await updateProfile(auth.currentUser, { displayName });

            // Update password if a new one is provided
            if (newPassword) {
                await updatePassword(auth.currentUser, newPassword);
            }

            // Update profile picture URL
            await updateProfile(auth.currentUser, { photoURL: profilePicUrl });

            navigate('/home');
        } catch (error) {
            console.error(error);
        }
    };

    return (
        <div className="container" style={{ display: 'flex', justifyContent: 'center', alignItems: 'flex-start', height: '100vh' }}>
            <div className="form-container" style={{ marginRight: '20px', height: '100%' }}>
                <form onSubmit={modifyProfile}>
                    <div className="field">
                        <label className="label">Adresse courriel</label>
                        <div className="control">
                            <input className="input" value={email} onChange={(e) => setEmail(e.target.value)} type="email" required />
                        </div>
                    </div>
                    <div className="field">
                        <label className="label">Nom d'affichage</label>
                        <div className="control">
                            <input className="input" value={displayName} onChange={(e) => setDisplayName(e.target.value)} type="text" required />
                        </div>
                    </div>
                    <div className="field">
                        <label className="label">Mot de passe courrant</label>
                        <div className="control">
                            <input className="input" value={currentPassword} onChange={(e) => setCurrentPassword(e.target.value)} type="password" required />
                        </div>
                    </div>
                    <div className="field">
                        <label className="label">Nouveau mot de passe</label>
                        <div className="control">
                            <input className="input" value={newPassword} onChange={(e) => setNewPassword(e.target.value)} type="password" />
                        </div>
                    </div>
                    <br />
                    <div className="control">
                        <button className="button is-primary">Modifier le profile</button>
                    </div>
                    <div>
                        <Link className="link" to="/" type="button">Annuler</Link>
                    </div>
                </form>
            </div>
        </div>
    );
};

export default ModifyProfileSettings;
