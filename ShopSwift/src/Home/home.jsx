import React, { useEffect, useState } from 'react';
import { collection, getDocs, getFirestore, query, where } from 'firebase/firestore';
import { Link } from 'react-router-dom';
import ClothingCard from "../components/ClothingCard.jsx";
import SearchInput from "../components/SearchInput.jsx";
import MaxPriceInput from "../components/MaxPriceInput.jsx";
import ClothingTypeSelect from "../components/ClothingTypeSelect.jsx";
import SizeSelect from "../components/SizeSelect.jsx";
import GenderSelect from "../components/Gender.jsx";

import 'bulma/css/bulma.min.css';

const Home = () => {
  const [clothes, setClothes] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
  const [maxPrice, setMaxPrice] = useState('');
  const [clothingType, setClothingType] = useState('');
  const [Gender, setGender] = useState('');
  const [size, setSize] = useState('');
  const [isShoe, setIsShoe] = useState(false);

  // Handler function for clothing type change
  const handleClothingTypeChange = (e) => {
    const selectedClothingType = e.target.value;
    setClothingType(selectedClothingType);
    // Check if shoes are selected
    setIsShoe(selectedClothingType === 'Chaussures');
  };

  const handleGenderChange = (e) => {
    const selectedGender = e.target.value;
    setGender(selectedGender);

  };

  useEffect(() => {
    // Fetch clothes data from Firestore based on filters
    const fetchClothes = async () => {
      try {
        const db = getFirestore();
        const clothesCollection = collection(db, 'vetements');
  
        // Build the query based on filters
        let clothesQuery = clothesCollection;
  
        if (searchTerm) {
          const searchTermLower = searchTerm.toLowerCase();
          clothesQuery = query(clothesQuery, where('productName', '>=', searchTermLower), where('productName', '<=', searchTermLower + '\uf8ff'));
        }
  
        if (clothingType) {
          clothesQuery = query(clothesQuery, where('clothingType', '==', clothingType));
        }
  
        if (size) {
          clothesQuery = query(clothesQuery, where('size', '==', size));
        }

        if (Gender) {
          clothesQuery = query(clothesQuery, where('gender', '==', Gender));
        }

        clothesQuery = query(clothesQuery, where('vendu', '==', false));
  
        // Fetch documents based on the query
        const clothesSnapshot = await getDocs(clothesQuery);
        const clothesData = clothesSnapshot.docs.map((doc) => ({
          id: doc.id,
          ...doc.data(),
        }));
  
        // Apply additional filtering locally
        let filteredClothes = clothesData;
        if (maxPrice) {
          const maxPriceNumber = parseFloat(maxPrice);
          filteredClothes = filteredClothes.filter(clothing => clothing.price <= maxPriceNumber);
        }
  
        // Update the state with the filtered clothes
        setClothes(filteredClothes);
      } catch (error) {
        console.error('Error fetching clothes:', error.message);
      }
    };
  
    fetchClothes();
  }, [searchTerm, maxPrice, clothingType, size,Gender]);
  

  return (
      <div className="container">
        {/* Search filters */}
        <div className="columns">
          <SearchInput value={searchTerm} onChange={(e) => setSearchTerm(e.target.value)} />
          <MaxPriceInput value={maxPrice} onChange={(e) => setMaxPrice(e.target.value)} />
          <ClothingTypeSelect value={clothingType} onChange={handleClothingTypeChange} />
          <SizeSelect value={size} onChange={(e) => setSize(e.target.value)} isShoe={isShoe} />
          <GenderSelect value={Gender} onChange={handleGenderChange} />
        </div>

        {/* Clothes display */}
        <div className="columns is-multiline">
          {clothes.map((clothing) => (
              <div key={clothing.id} className="column is-one-third">
                <Link to={`/product/${clothing.id}`}>
                  <ClothingCard clothing={clothing} />
                </Link>
              </div>
          ))}
        </div>
      </div>
  );
};

export default Home;
