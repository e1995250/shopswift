import React, { useState, useEffect } from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { getAuth, signOut, onAuthStateChanged } from 'firebase/auth';
import 'bulma/css/bulma.min.css';
import '@fortawesome/fontawesome-free/css/all.min.css';

const Navbar = () => {
  const location = useLocation();
  const navigate = useNavigate();
  const [user, setUser] = useState(null);
  const [isActive, setIsActive] = useState(false);
  const [isProfileDropdownActive, setIsProfileDropdownActive] = useState(false); // State to manage profile dropdown

  useEffect(() => {
    const auth = getAuth();
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      setUser(user);
    });

    return () => {
      unsubscribe();
    };
  }, []);

  const handleLogout = async () => {
    try {
      const auth = getAuth();
      await signOut(auth);
      navigate('/login', { replace: true });
    } catch (error) {
      console.error('Error logging out:', error.message);
    }
  };

  if (!user || location.pathname === '/login' || location.pathname === '/signup') {
    return null; // Don't render Navbar if user is not logged in or on login/signup pages
  }

  const toggleProfileDropdown = () => {
    setIsProfileDropdownActive(!isProfileDropdownActive);
  };

  const toggleBurgerMenu = () => {
    setIsActive(!isActive);
    if (!isActive) {
      setIsProfileDropdownActive(false); // Close profile dropdown when burger menu is clicked to open
    }
  };

  return (
      <nav className="navbar" role="navigation" aria-label="main navigation">
        <div className="navbar-brand">
          <Link to="/home" className="navbar-item">
            <strong>Shopswift</strong>
          </Link>
          <button
              className={`navbar-burger burger ${isActive ? 'is-active' : ''}`}
              aria-label="menu"
              aria-expanded="false"
              data-target="navbarBasicExample"
              onClick={toggleBurgerMenu}
          >
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </button>
        </div>

        <div id="navbarBasicExample" className={`navbar-menu ${isActive ? 'is-active' : ''}`}>
          <div className="navbar-start">
            <Link to="/vendre" className="navbar-item">
              Vendre
            </Link>
            <Link to="/cart" className="navbar-item">
            <span className="icon">
              <i className="fas fa-shopping-cart"></i>
            </span>
              &nbsp; Panier
            </Link>
          </div>

          <div className="navbar-end">
            <div className={`navbar-item has-dropdown ${isProfileDropdownActive ? 'is-active' : ''}`}>
            <span className="navbar-link" onClick={toggleProfileDropdown}>
              <span className="icon">
                <i className="fas fa-user"></i>
              </span>
            </span>

              <div className="navbar-dropdown">
                <Link to="/settings" className="navbar-item">
                  Paramètres
                </Link>
                <Link to="/mystore" className="navbar-item">
                    Ma boutique
                </Link>
                <Link to="/mes_achats" className="navbar-item">
                    Mes achats
                </Link>
                <Link to="/mes_ventes" className="navbar-item">
                    Mes ventes
                </Link>
              </div>
            </div>
            <div className="navbar-item">
              <div className="buttons">
                <button className="button is-danger" onClick={handleLogout}>
                  Déconnexion
                </button>
              </div>
            </div>
          </div>
        </div>
      </nav>
  );
};

export default Navbar;
