import { useState } from 'react';
import { createUserWithEmailAndPassword, sendEmailVerification } from 'firebase/auth';
import { getFirestore, doc, setDoc } from 'firebase/firestore'; // Import necessary Firestore functions
import 'bulma/css/bulma.min.css';
import { auth } from '../index.jsx';
import { useNavigate } from 'react-router-dom';

const Signup = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState(null); // State variable for error message
  const navigate = useNavigate();

  const handleSignup = async (e) => {
    e.preventDefault();

    try {
      const userCredential = await createUserWithEmailAndPassword(auth, email, password);

      // Create a document in the "Panier" collection with the user's UID and an empty "products" array
      const db = getFirestore();
      const panierRef = doc(db, 'Panier', userCredential.user.uid);
      await setDoc(panierRef, { products: [] });

      await sendEmailVerification(userCredential.user);
      console.log('User registered successfully! Confirmation email sent.');
      navigate('/login');

    } catch (error) {
      // Update error state with the error message received from Firebase
      setError(error.message);
      console.error('Error signing up:', error.message);
    }
  };


  return (
      <div className="hero is-fullheight is-light">
        <div className="hero-body">
          <div className="container">
            <div className="columns is-centered">
              <div className="column is-one-third">
                <div className="box">
                  <h2 className="title is-3 has-text-centered mb-4">S'inscrire</h2>
                  {error && ( // Render error message if it exists
                      <div className="notification is-danger is-light mb-4">{error}</div>
                  )}
                  <form onSubmit={handleSignup}>
                    <div className="field">
                      <label className="label">Adresse courriel</label>
                      <div className="control">
                        <input className="input is-medium" type="email" value={email} onChange={(e) => setEmail(e.target.value)} />
                      </div>
                    </div>
                    <div className="field">
                      <label className="label">Mot de passe</label>
                      <div className="control">
                        <input className="input is-medium" type="password" value={password} onChange={(e) => setPassword(e.target.value)} />
                      </div>
                    </div>
                    <div className="field">
                      <div className="control has-text-centered">
                        <button className="button is-primary is-medium" type="submit">S'inscrire</button>
                      </div>
                    </div>
                  </form>
                  <div className="has-text-centered mt-4">
                    <p>Vous avez déjà un compte?
                    </p>
                    <a className="button is-info" href="/Login">Se connecter</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  );
};

export default Signup;
