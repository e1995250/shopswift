import { useState, useEffect } from 'react';
import { getAuth, signInWithEmailAndPassword } from 'firebase/auth';
import 'bulma/css/bulma.min.css';
import { useNavigate } from 'react-router-dom'; // Import useNavigate

const Login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loginError, setLoginError] = useState('');
  const navigate = useNavigate(); // Use useNavigate for version 6

  const auth = getAuth();

  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged(async (user) => {
      if (user && user.emailVerified) {
        navigate('/home');
      }
    });

    return () => unsubscribe();
  }, [auth, navigate]);

  const handleLogin = async (e) => {
    e.preventDefault();

    const auth = getAuth();

    try {
      const userCredential = await signInWithEmailAndPassword(auth, email, password);
      const user = userCredential.user;

      if (user.emailVerified) {
        console.log('Logged in successfully!');
        navigate('/home'); // Redirect to home if email is verified
      } else {
        setLoginError('Email not verified. Please check your inbox and verify your email to login.'); // Set login error
      }
    } catch (error) {
      console.error('Error logging in:', error.message);
      setLoginError('Invalid email or password. Please try again.'); // Set login error
    }
  };

  return (
      <div className="hero is-fullheight is-light">
        <div className="hero-body">
          <div className="container">
            <div className="columns is-centered">
              <div className="column is-one-third">
                <div className="box">
                  <h2 className="title is-3 has-text-centered mb-4">Se connecter</h2>
                  {loginError && (
                      <div className="notification is-danger">
                        {loginError}
                      </div>
                  )}
                  <form onSubmit={handleLogin}>
                    <div className="field">
                      <label className="label">Adresse courriel</label>
                      <div className="control">
                        <input className="input is-medium" type="email" value={email} onChange={(e) => setEmail(e.target.value)} />
                      </div>
                    </div>
                    <div className="field">
                      <label className="label">Mot de passe</label>
                      <div className="control">
                        <input className="input is-medium" type="password" value={password} onChange={(e) => setPassword(e.target.value)} />
                      </div>
                    </div>
                    <div className="field">
                      <div className="control has-text-centered">
                        <button className="button is-primary is-medium" type="submit">Se connecter</button>
                      </div>
                    </div>
                  </form>
                  <div className="has-text-centered mt-4">
                    <p>Vous n'avez pas de compte?</p>
                    <a className="button is-info" href="/signup">S'inscrire</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  );
};

export default Login;
