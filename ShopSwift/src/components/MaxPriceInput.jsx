import React from 'react';

const MaxPriceInput = ({ value, onChange }) => {
    return (
        <div className="column">
            <input
                className="input"
                type="number"
                placeholder="Prix maximum"
                value={value}
                onChange={onChange}
            />
        </div>
    );
};

export default MaxPriceInput;
