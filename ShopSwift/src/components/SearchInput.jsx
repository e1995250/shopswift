import React from 'react';

const SearchInput = ({ value, onChange }) => {
    return (
        <div className="column">
            <input
                className="input"
                type="text"
                placeholder="Recherche par nom de produit"
                value={value}
                onChange={onChange}
            />
        </div>
    );
};

export default SearchInput;
