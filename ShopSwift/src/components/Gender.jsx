import React from 'react';

const GenderSelect = ({ value, onChange }) => {
    const handleSelectChange = (e) => {
        onChange(e); 
    };

    return (
        <div className="column">
            <div className="select">
                <select
                    value={value}
                    onChange={handleSelectChange}
                >
                    <option value="">Sélectionner le Genre</option>
                    <option value="Homme">Homme</option>
                    <option value="Femme">Femme</option>
                    <option value="Unisexe">Unisexe</option>
                </select>
            </div>
        </div>
    );
};


export default GenderSelect;
