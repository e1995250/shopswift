import React from 'react';
import { Link } from 'react-router-dom';

const clothingCard = ({ clothing }) => (
    <div className="card">
        <div className="card-image">
            <figure className="image is-4by3">
                <img src={clothing.imagesUrls[0]} alt={clothing.productName} />
            </figure>
        </div>
        <div className="card-content">
            <p className="title is-4">{clothing.productName}</p>
            <p className="description has-text-overflow-ellipsis">{clothing.description}</p>
            <p>Type: {clothing.clothingType}</p>
            <p>Genre: {clothing.gender}</p>
            <p>Taille: {clothing.size}</p>
            <p>Prix: {clothing.price}</p>
        </div>
    </div>
);

export default clothingCard;
