import React from 'react';

const ClothingTypeSelect = ({ value, onChange }) => {
    const handleSelectChange = (e) => {
        onChange(e); // Update clothing type
    };

    return (
        <div className="column">
            <div className="select">
                <select
                    value={value}
                    onChange={handleSelectChange}
                >
                    <option value="">Sélectionner le type</option>
                    <option value="Pantalon">Pantalon</option>
                    <option value="Chemise">Chemise</option>
                    <option value="Robe">Robe</option>
                    <option value="Veste">Veste</option>
                    <option value="Pull">Pull</option>
                    <option value="Jupe">Jupe</option>
                    <option value="Shorts">Shorts</option>
                    <option value="Blouse">Blouse</option>
                    <option value="Manteau">Manteau</option>
                    <option value="Écharpe">Écharpe</option>
                    <option value="Gants">Gants</option>
                    <option value="Chapeau">Chapeau</option>
                    <option value="Chaussettes">Chaussettes</option>
                    <option value="Chaussures">Chaussures</option>
                </select>
            </div>
        </div>
    );
};


export default ClothingTypeSelect;
