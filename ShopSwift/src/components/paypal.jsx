import React, { useEffect, useRef, useState } from "react";
import { useNavigate } from "react-router-dom";
import { collection, doc, getFirestore, serverTimestamp, setDoc, updateDoc } from "firebase/firestore";
import { getAuth, onAuthStateChanged } from "firebase/auth";

const Paypal = ({ cart, shippingAddress }) => {
    const paypal = useRef();
    const [sdkLoaded, setSdkLoaded] = useState(false);
    const navigate = useNavigate();
    const [currentUser, setCurrentUser] = useState(null);

    useEffect(() => {
        const script = document.createElement("script");
        script.src = "https://www.paypal.com/sdk/js?client-id=AdG5oQwoI-8kBmHEz976erNLIfJ6fRHSRL18slE2vUGshsQM49FZVqaE8Bxau93NZzyVXYonpD00BopY&currency=CAD";
        script.dataset.namespace = "paypal_sdk"; // Specify the namespace
        script.addEventListener("load", () => setSdkLoaded(true));
        document.body.appendChild(script);

        return () => {
            document.body.removeChild(script);
        };
    }, []);

    useEffect(() => {
        const auth = getAuth();
        return onAuthStateChanged(auth, (user) => {
            setCurrentUser(user);
        });
    }, []);

    const getTotalAmount = (cart) => {
        return cart.reduce((total, item) => total + item.price, 0);
    };

    const handleCheckout = async () => {
        try {
            const db = getFirestore();
            const batch = [];

            // Update vendu field for each product in the cart
            for (const product of cart) {
                const productRef = doc(db, 'vetements', product.id);
                batch.push(updateDoc(productRef, { vendu: true }));
            }

            // Add transactions to the Transactions collection
            for (const product of cart) {
                const transactionsCollection = collection(db, 'Transactions');
                const transactionRef = doc(transactionsCollection); // Automatically generates a new document ID
                const transactionData = {
                    buyerId: currentUser.uid,
                    sellerId: product.userId,
                    done: false,
                    productId: product.id,
                    createdAt: serverTimestamp(),
                    shippingDetails: shippingAddress // Here we add the shipping details
                };
                batch.push(setDoc(transactionRef, transactionData));
            }

            // Execute all batched operations
            await Promise.all(batch);

            // Clear the user's cart
            const panierRef = doc(db, 'Panier', currentUser.uid);
            await updateDoc(panierRef, { products: [] });

            // Navigate to the home page or any other page you desire
            navigate('/home');
        } catch (error) {
            console.error('Error during checkout:', error.message);
        }
    };

    useEffect(() => {
        if (sdkLoaded && currentUser) {
            // Remove the old PayPal button
            while (paypal.current.firstChild) {
                paypal.current.firstChild.remove();
            }

            // Render the new PayPal button
            window.paypal_sdk
                .Buttons({
                    createOrder: (data, actions) => {
                        const subtotal = getTotalAmount(cart);
                        const deliveryFee = 15;
                        const tpsTvh = subtotal * 0.05;
                        const tvpTvdTvq = subtotal * 0.09975;
                        const total = subtotal + deliveryFee + tpsTvh + tvpTvdTvq;

                        return actions.order.create({
                            intent: "CAPTURE",
                            purchase_units: [
                                {
                                    description: "Shop Swift Order",
                                    amount: {
                                        currency_code: "CAD",
                                        value: total.toFixed(2),
                                    },
                                },
                            ],
                        });
                    },
                    onApprove: async (data, actions) => {
                        const order = await actions.order.capture();
                        console.log(order);

                        handleCheckout();
                    },
                    onError: (err) => {
                        console.log(err);
                    },
                })
                .render(paypal.current);
        }
    }, [sdkLoaded, cart, currentUser, shippingAddress]);

    return <div ref={paypal}></div>;
};

export default Paypal;
