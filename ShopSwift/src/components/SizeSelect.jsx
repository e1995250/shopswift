import React from 'react';

const SizeSelect = ({ value, onChange, isShoe }) => {
    return (
        <div className="column">
            <div className="select">
                <select
                    value={value}
                    onChange={onChange}
                >
                    <option value="">Sélectionner la taille</option>
                    {isShoe ? (
                        <>
                            <option value="7">7 (US)</option>
                            <option value="7.5">7.5 (US)</option>
                            <option value="8">8 (US)</option>
                            <option value="8.5">8.5 (US)</option>
                            <option value="9">9 (US)</option>
                            <option value="9.5">9.5 (US)</option>
                            <option value="10">10 (US)</option>
                            <option value="10.5">10.5 (US)</option>
                            <option value="11">11 (US)</option>
                            <option value="11.5">11.5 (US)</option>
                            <option value="12">12 (US)</option>
                            <option value="12.5">12.5 (US)</option>
                        </>
                    ) : (
                        <>
                            <option value="XS">XS</option>
                            <option value="S">S</option>
                            <option value="M">M</option>
                            <option value="L">L</option>
                            <option value="XL">XL</option>
                        </>
                    )}
                </select>
            </div>
        </div>
    );
};


export default SizeSelect;
