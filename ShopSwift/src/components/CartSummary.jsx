import React from "react";

const CartSummary = ({cart}) => {
    return (
        <div>
            {cart.map((product) => (
                    <div className="card" style={{ display: 'flex', alignItems: 'center', cursor: 'pointer' }}>
                        <div className="card-image">
                            <figure className="image is-4by3">
                                <img src={product.imagesUrls[0]} alt={product.productName} />aaaaaaaaaaaaaaaaaaaaaaaaa
                            </figure>
                        </div>
                        <div className="card-content" style={{ height: '200px' }}>
                            <p className="title is-5">{product.productName}</p>
                            <p>Prix: ${product.price}</p>
                            <p>Taille: {product.size}</p>
                            <p>Type: {product.clothingType}</p>
                            <p>Genre: {product.gender}</p>
                        </div>
                    </div>
            ))}
        </div>
    );
};

export default CartSummary;
