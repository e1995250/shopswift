import React, { useEffect, useState } from "react";
import Paypal from "../components/paypal.jsx";
import { getAuth, onAuthStateChanged } from "firebase/auth";
import { doc, getDoc, getFirestore } from "firebase/firestore";
import CartSummary from "../components/CartSummary.jsx";

const Checkout = () => {
    const [shippingDetails, setShippingDetails] = useState({
        fullName: "",
        address: "",
        city: "",
        postalCode: "",
        country: ""
    });
    const [cart, setCart] = useState([]);
    const [currentUser, setCurrentUser] = useState(null);
    const [shippingAddress, setShippingAddress] = useState("");

    useEffect(() => {
        const fetchCart = async () => {
            try {
                const auth = getAuth();
                const user = await new Promise((resolve) => {
                    const unsubscribe = onAuthStateChanged(auth, (user) => {
                        resolve(user);
                        unsubscribe();
                    });
                });

                setCurrentUser(user);

                if (user) {
                    const db = getFirestore();
                    const panierRef = doc(db, 'Panier', user.uid);
                    const panierDoc = await getDoc(panierRef);

                    if (panierDoc.exists()) {
                        const cartProducts = panierDoc.data().products || [];
                        const productDetails = await Promise.all(cartProducts.map(async (productId) => {
                            const productRef = doc(db, 'vetements', productId);
                            const productDoc = await getDoc(productRef);
                            return { id: productId, ...productDoc.data() };
                        }));

                        setCart(productDetails);
                    } else {
                        console.log('No cart found for the current user.');
                        setCart([]);
                    }
                }
            } catch (error) {
                console.error('Error fetching cart:', error.message);
            }
        };

        fetchCart();
    }, []);

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setShippingDetails({ ...shippingDetails, [name]: value });
    };

    useEffect(() => {
        const { address, city, country, postalCode } = shippingDetails;
        const concatenatedAddress = `${address}, ${city}, ${country} ${postalCode}`;
        setShippingAddress(concatenatedAddress);
    }, [shippingDetails]);

    const subtotal = cart.reduce((total, product) => total + (product.price || 0), 0);
    const totalItems = cart.length;

    return (
        <div style={{ display: "flex", justifyContent: "center", padding: "20px" }}>
            <div style={{ maxWidth: "1000px", width: "100%" }}>
                <h1 style={{ textAlign: "center", marginBottom: "30px" }}>Vérifier votre achat</h1>
                <div style={{ display: "flex", justifyContent: "space-between" }}>
                    {/* Left Column */}
                    <div style={{ flexBasis: "50%", marginRight: "20px" }}>
                        {/* Address Information */}
                        <div style={{ marginBottom: "30px" }}>
                            <h2 className="has-text-centered">Détails d'expédition</h2>
                            <hr />
                            <form>
                                <div style={{ marginBottom: "15px" }}>
                                    <label htmlFor="fullName">Nom complet:</label>
                                    <input type="text" id="fullName" name="fullName" value={shippingDetails.fullName}
                                           onChange={handleInputChange} style={{ width: "100%" }} required />
                                </div>
                                <div style={{ marginBottom: "15px" }}>
                                    <label htmlFor="address">Adresse:</label>
                                    <input type="text" id="address" name="address" value={shippingDetails.address}
                                           onChange={handleInputChange} style={{ width: "100%" }} required />
                                </div>
                                <div style={{ marginBottom: "15px" }}>
                                    <label htmlFor="city">Ville:</label>
                                    <input type="text" id="city" name="city" value={shippingDetails.city}
                                           onChange={handleInputChange} style={{ width: "100%" }} required />
                                </div>
                                <div style={{ marginBottom: "15px" }}>
                                    <label htmlFor="postalCode">Code postal:</label>
                                    <input type="text" id="postalCode" name="postalCode"
                                           value={shippingDetails.postalCode} onChange={handleInputChange}
                                           style={{ width: "100%" }} required />
                                </div>
                                <div style={{ marginBottom: "15px" }}>
                                    <label htmlFor="country">Pays:</label>
                                    <input type="text" id="country" name="country" value={shippingDetails.country}
                                           onChange={handleInputChange} style={{ width: "100%" }} required />
                                </div>
                            </form>
                        </div>

                        {/* Paypal Renderer */}
                        <div>
                            <h2 className="has-text-centered">Méthode de paiement</h2>
                            <hr />
                            <Paypal cart={cart} shippingAddress={shippingAddress} />
                        </div>
                    </div>
                    {/* Right Column */}
                    <div style={{ flexBasis: "45%" }}>
                        <h2 className="has-text-centered">Panier</h2>
                        <hr />
                        {/* Cart and Summary */}
                        <div style={{ marginBottom: "30px", maxHeight: "400px", overflowY: "auto" }}>
                            <CartSummary cart={cart} />
                        </div>
                        {/* Order Summary */}
                        <div>
                            <p>Articles ({totalItems}): ${subtotal.toFixed(2)}</p>
                            <p>Sous-total: ${subtotal.toFixed(2)}</p>
                            <p>Frais de livraison: $15</p>
                            <p>TPS/TVH estimée: ${(subtotal * 0.05).toFixed(2)}</p>
                            <p>TVP/TVD/TVQ estimée: ${(subtotal * 0.09975).toFixed(2)}</p>
                            <p>Total: ${(subtotal + 15 + (subtotal * 0.05) + (subtotal * 0.09975)).toFixed(2)}</p>
                            <hr/>
                            <p>Montant total: ${(subtotal + 15 + (subtotal * 0.05) + (subtotal * 0.09975)).toFixed(2)}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Checkout;
