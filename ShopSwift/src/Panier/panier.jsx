import React, { useState, useEffect } from 'react';
import { getFirestore, doc, getDoc, arrayRemove, updateDoc } from 'firebase/firestore';
import { getAuth, onAuthStateChanged } from 'firebase/auth';
import { Link } from 'react-router-dom';
import 'bulma/css/bulma.min.css';

const CartPage = () => {
  const [cart, setCart] = useState([]);
  const [currentUser, setCurrentUser] = useState(null);

  useEffect(() => {
    const fetchCart = async () => {
      try {
        const auth = getAuth();
        const user = await new Promise((resolve) => {
          const unsubscribe = onAuthStateChanged(auth, (user) => {
            resolve(user);
            unsubscribe();
          });
        });

        setCurrentUser(user);

        if (user) {
          const db = getFirestore();
          const panierRef = doc(db, 'Panier', user.uid);
          const panierDoc = await getDoc(panierRef);

          if (panierDoc.exists()) {
            const cartProducts = panierDoc.data().products || [];
            const productDetails = await Promise.all(cartProducts.map(async (productId) => {
              const productRef = doc(db, 'vetements', productId);
              const productDoc = await getDoc(productRef);
              return { id: productId, ...productDoc.data() };
            }));

            setCart(productDetails);
          } else {
            console.log('No cart found for the current user.');
            setCart([]);
          }
        }
      } catch (error) {
        console.error('Error fetching cart:', error.message);
      }
    };

    fetchCart();
  }, []);

  const handleDelete = async (productId) => {
    try {
      const db = getFirestore();
      const panierRef = doc(db, 'Panier', currentUser.uid);

      await updateDoc(panierRef, {
        products: arrayRemove(productId),
      });

      const updatedPanierDoc = await getDoc(panierRef);
      const updatedCartProducts = updatedPanierDoc.data().products || [];
      const updatedProductDetails = await Promise.all(updatedCartProducts.map(async (id) => {
        const productRef = doc(db, 'vetements', id);
        const productDoc = await getDoc(productRef);
        return { id, ...productDoc.data() };
      }));

      setCart(updatedProductDetails);
    } catch (error) {
      console.error('Error deleting product:', error.message);
    }
  };

  // Calculate subtotal
  const subtotal = cart.reduce((total, product) => total + (product.price || 0), 0);
  const totalItems = cart.length;

  return (
      <div className="container">
        {currentUser ? (
            <div>
              {cart.length > 0 ? (
                  <div>
                    <h2 className="subtitle is-4">Votre panier:</h2>
                    {cart.map((product) => (
                        <Link to={`/product/${product.id}`} key={product.id}><div className="card" style={{ display: 'flex', alignItems: 'center', cursor: 'pointer' }}>
                          <div className="card-image">
                            <figure className="image is-4by3">
                              <img src={product.imagesUrls[0]} alt={product.productName} />aaaaaaaaaaaaaaaaaaaaaaaaaaaa
                            </figure>
                          </div>
                          <div className="card-content" style={{ height: '200px' }}>
                            <p className="title is-5">{product.productName}</p>
                            <p>Price: ${product.price}</p>
                            <p>Size: {product.size}</p>
                            <p>Type: {product.clothingType}</p>
                            <p>Genre: {product.gender}</p>
                          </div>
                          <footer className="card-footer is-justify-content-space-between">
                            <a href="#" className="card-footer-item" onClick={(e) => { e.stopPropagation(); handleDelete(product.id); }}>
                                Supprimer
                            </a>
                          </footer>
                        </div>

                        </Link>
                    ))}
                    <p className="mt-4">Total partiel ({totalItems} articles): ${subtotal.toFixed(2)}</p>
                    <div className="buttons mt-4">
                      <Link to="/checkout" className="button is-primary">Passer la commande</Link>
                    </div>
                  </div>
              ) : (
                  <p>Votre panier est vide.</p>
              )}
            </div>
        ) : (
            <p>Veuillez vous connecter pour voir votre panier.</p>
        )}
      </div>
  );
};

export default CartPage;

