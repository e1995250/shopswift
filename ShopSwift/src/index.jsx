// firebase.js
import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';

const firebaseConfig = {
    apiKey: "AIzaSyCPUz4SeXeY8ofV9DLSF09QCO-yay70A10",
    authDomain: "shopswift-253f4.firebaseapp.com",
    projectId: "shopswift-253f4",
    storageBucket: "shopswift-253f4.appspot.com",
    messagingSenderId: "649615693836",
    appId: "1:649615693836:web:28ceee61c1f592abedf629"
  };

const app = initializeApp(firebaseConfig);
const auth = getAuth(app);

export { auth };
